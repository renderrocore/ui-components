/* eslint-disable import/prefer-default-export */
export { default as RBadge } from './RBadge.vue';
export { default as RInput } from './RInput.vue';
export { default as RLoader } from './RLoader.vue';
export { default as RToggle } from './RToggle.vue';
export { default as RButton } from './RButton.vue'

// icons
export { default as RIcon } from './RIcon/BaseIcon.vue';
export { default as ArchiveIcon } from './RIcon/Archive.vue';
export { default as ArrowLeftIcon } from './RIcon/ArrowLeft.vue';
export { default as CardIcon } from './RIcon/Card.vue';
export { default as CheckmarkIcon } from './RIcon/Checkmark.vue';
export { default as CloseIcon } from './RIcon/Close.vue';
export { default as DashboardIcon } from './RIcon/Dashboard.vue';
export { default as DirIcon } from './RIcon/Dir.vue';
export { default as DriveIcon } from './RIcon/Drive.vue';
export { default as EyeIcon } from './RIcon/Eye.vue';
export { default as EyeCrossedIcon } from './RIcon/EyeCrossed.vue';
export { default as FileIcon } from './RIcon/File.vue';
export { default as GearIcon } from './RIcon/Gear.vue';
export { default as GoogleIcon } from './RIcon/Google.vue';
export { default as HelpIcon } from './RIcon/Help.vue';
export { default as InfoIcon } from './RIcon/Info.vue';
export { default as LogoIcon } from './RIcon/Logo.vue';
export { default as MoreIcon } from './RIcon/More.vue';
export { default as NotificationSuccessIcon } from './RIcon/NotificationSuccess.vue';
export { default as RatesIcon } from './RIcon/Rates.vue';
export { default as RFlagIcon } from './RIcon/RFlag.vue';
export { default as ShieldIcon } from './RIcon/Shield.vue';
export { default as ShuffleIcon } from './RIcon/Shuffle.vue';
export { default as TeamsIcon } from './RIcon/Teams.vue';
export { default as WorkstationIcon } from './RIcon/Workstation.vue';
