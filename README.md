# Renderro UI Components

This package contains front end code shared across Renderro products and modularized into visual components.

Currently it consists of:
- RButton
- RBadge
- RInput
- RLoader
- RToggle
- RIcon

## Development

`npm run serve`

The dev server is `vue/cli-service` (and, therefore, webpack) based. We use it to summon the `dev/serve.vue` parent to display our components.

## Building

`npm run build`

We use Rollup (at `build/rollup.config.js`) to build to different targets (`ESM`, `CommonJS` and `IIFE`). To inject global color variables, we use `rollup-plugin-vue-global-styles`.

## Consuming the package

`npm install @renderro/ui-components`

To utilize the package in a TypeScript project, you need to declare it as a module, like so:
```
// in a ui-components.d.ts, put where your ts can see it
declare module 'ui-components'
```
## FAQ

### How to import the color palette?
Colors are encoded as SCSS variables. To have them available in your project, same as the package uses them, firstly you need to have SASS incorporated into your workflow, then you need to import the `variables.scss` into your styles, probably by altering the `sass-loader` configuration. This is how it looks for `vue/cli-service` / `webpack`:
```
// vue.config.js
module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "~@renderro/ui-components/styles/variables.scss";`,
      },
    },
  },
}
```

### I am getting an 'eslint config missing' error
If you are using a vue-cli generated project, consider disabling webpack symlinks resolution adding the following line to your `vue.config.js`:

```
module.exports = {
  ...
  chainWebpack: config => config.resolve.symlinks(false),
	...
}
```