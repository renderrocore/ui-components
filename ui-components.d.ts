import Vue, { PluginFunction, VueConstructor } from 'vue';

interface InstallFunction extends PluginFunction<any> {
  installed?: boolean;
}

declare const RenderroComponents: { install: InstallFunction };
export default RenderroComponents;

export const RButton: VueConstructor<Vue>;
export const RInput: VueConstructor<Vue>;
export const RLoader: VueConstructor<Vue>;
export const RToggle: VueConstructor<Vue>;
export const RBadge: VueConstructor<Vue>;

export const RIcon: VueConstructor<Vue>;
export const ArchiveIcon: VueConstructor<Vue>;
export const ArrowLeftIcon: VueConstructor<Vue>;
export const CardIcon: VueConstructor<Vue>;
export const CheckmarkIcon: VueConstructor<Vue>;
export const CloseIcon: VueConstructor<Vue>;
export const DashboardIcon: VueConstructor<Vue>;
export const DirIcon: VueConstructor<Vue>;
export const DriveIcon: VueConstructor<Vue>;
export const EyeIcon: VueConstructor<Vue>;
export const EyeCrossedIcon: VueConstructor<Vue>;
export const FileIcon: VueConstructor<Vue>;
export const GearIcon: VueConstructor<Vue>;
export const GoogleIcon: VueConstructor<Vue>;
export const HelpIcon: VueConstructor<Vue>;
export const InfoIcon: VueConstructor<Vue>;
export const LogoIcon: VueConstructor<Vue>;
export const MoreIcon: VueConstructor<Vue>;
export const NotificationSuccessIcon: VueConstructor<Vue>;
export const RatesIcon: VueConstructor<Vue>;
export const RFlagIcon: VueConstructor<Vue>;
export const ShieldIcon: VueConstructor<Vue>;
export const ShuffleIcon: VueConstructor<Vue>;
export const TeamsIcon: VueConstructor<Vue>;
export const WorkstationIcon: VueConstructor<Vue>;
