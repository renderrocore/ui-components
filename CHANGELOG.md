### 0.0.6

- Add transitions to RButton;
- Fix icon export problem;

### 0.0.5

- Rename icons so that the `Icon` is a suffix rather than prefix;
- Fix file declaration bug;

### 0.0.3

- Redo icons as per [Vue Cookbook](https://vuejs.org/v2/cookbook/editable-svg-icons.html).
  Doing code reuse through a slot instead of a dynamic import helps with packing.