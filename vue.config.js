// vue.config.js
// we still use vue-service for serving components locally.
// we need to ensure they all have the import added 
module.exports = {
  configureWebpack: {
    devtool: 'source-map',
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/styles/breakpoints.scss";
@import "@/styles/colors.scss";`,
      },
    },
  },
}
  